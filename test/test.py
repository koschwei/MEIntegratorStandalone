import ROOT
import numpy as np
ROOT.gSystem.Load("libFWCoreFWLite.so")
ROOT.gSystem.Load("libTTHMEIntegratorStandalone.so")
from ROOT import MEM
from ROOT import TLorentzVector
import math

cfg = ROOT.MEM.MEMConfig()
cfg.defaultCfg()
cfg.save_permutations = True
cfg.transfer_function_method = MEM.TFMethod.Builtin
#cfg.int_code = sum([getattr(MEM.IntegrandType, x) for x in ["Jacobian", "Transfer", "ScattAmpl", "DecayAmpl"]])
#mem = MEM.Integrand(MEM.output+MEM.input+MEM.init+MEM.init_more+MEM.integration, cfg)
mem = MEM.Integrand(2, cfg)

def normalize_proba(vec):
    proba_vec = np.array(vec)
    proba_vec[proba_vec <= 1E-50] = 1E-50
    ret = np.array(np.log10(proba_vec), dtype="float64")
    return ret

def add_obj(mem, typ, **kwargs):

    if kwargs.has_key("p4s"):
        pt, eta, phi, mass = kwargs.pop("p4c")
        v = TLorentzVector()
        v.SetPtEtaPhiM(pt, eta, phi, mass);
    elif kwargs.has_key("p4c"):
        v = TLorentzVector(*kwargs.pop("p4c"))
    obsdict = kwargs.pop("obsdict", {})

    o = MEM.Object(v, typ)

    t1 = kwargs.get("tf", None)

    if t1 != None:
        o.addTransferFunction(MEM.TFType.qReco, t1)
        o.addTransferFunction(MEM.TFType.bReco, t1)

        o.addTransferFunction(MEM.TFType.qLost, t1)
        o.addTransferFunction(MEM.TFType.bLost, t1)

    for k, v in obsdict.items():
        o.addObs(k, v)
    mem.push_back_object(o)

t1 = None

add_obj(mem,
    MEM.ObjectType.Jet, p4c=(0, 50, 20, math.sqrt(50*50+20*20)),
    obsdict={MEM.Observable.BTAG: 1.0}, tf=t1
)

add_obj(mem,
    MEM.ObjectType.Jet, p4c=(70, 20, 10, math.sqrt(70*70+20*20+10*10)),
    obsdict={MEM.Observable.BTAG: 1.0}, tf=t1
)
add_obj(mem,
    MEM.ObjectType.Jet, p4c=(20, 50, 10, math.sqrt(20*20+50*50+10*10)),
    obsdict={MEM.Observable.BTAG: 1.0}, tf=t1
)
add_obj(mem,
    MEM.ObjectType.Jet, p4c=(100, 10, 20, math.sqrt(100*100 + 10*10 + 20*20)),
    obsdict={MEM.Observable.BTAG: 1.0}, tf=t1
)

add_obj(mem,
    MEM.ObjectType.Lepton, p4c=(70, 10, 20, math.sqrt(70*70+20*20+10*10)),
    obsdict={MEM.Observable.CHARGE: 1.0}
)
add_obj(mem,
    MEM.ObjectType.Lepton, p4c=(70, -10, -20, math.sqrt(70*70+20*20+10*10)),
    obsdict={MEM.Observable.CHARGE: -1.0}
)
add_obj(mem,
    MEM.ObjectType.MET, p4c=(30, 0, 0, 30),
)
CvectorPermutations = getattr(ROOT, "std::vector<MEM::Permutations::Permutations>")

pvec = CvectorPermutations()
pvec.push_back(MEM.Permutations.BTagged)
pvec.push_back(MEM.Permutations.QUntagged)
cfg.perm_pruning = pvec

CvectorPSVar = getattr(ROOT, "std::vector<MEM::PSVar::PSVar>")
vars_to_integrate   = CvectorPSVar()
vars_to_marginalize = CvectorPSVar()
r = mem.run(MEM.FinalState.LL, MEM.Hypothesis.TTH, vars_to_integrate, vars_to_marginalize)

Cvectordouble = getattr(ROOT, "std::vector<double>")
arr = np.array([0.1, 0.2, 0.3, 0.4, 0.0, 0.3], dtype=np.float64)
derivs = Cvectordouble()
variations = Cvectordouble()
out = getattr(ROOT, "MEM::MEMOutput")()
mem.init(MEM.FinalState.LL, MEM.Hypothesis.TTH)
mem.make_assumption(vars_to_integrate, vars_to_marginalize, out)
p = mem.Eval(arr, derivs, variations)
print "tth", r.p, p
#r = mem.run(MEM.FinalState.LL, MEM.Hypothesis.TTBB, vars_to_integrate, vars_to_marginalize)
#mem.next_event()
