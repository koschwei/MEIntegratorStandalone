#include "TTH/MEIntegratorStandalone/interface/Integrand.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/GSLMCIntegrator.h"
#include "Math/IOptions.h"
#include "Math/IntegratorOptions.h"
#include "Math/AllIntegrationTypes.h"
#include "TFile.h"
#include <iostream>

using namespace std;
using namespace MEM;

const auto npoints = 1000; //use default ncalls
const auto MAX_TIMES = 1;

// Returns the transfer function corresponding to a jet flavour and eta
TF1* getTransferFunction(TFile* tffile, const char* flavour, double eta) {
    int etabin = 0;
    if (std::abs(eta) > 1.0) {
        etabin = 1;
    }
    stringstream ss;
    ss << "tf_" << flavour << "_etabin" << etabin;
    const char* fname = ss.str().c_str();
    TF1* tf = (TF1*)(tffile->Get(fname));
    if (tf == 0) {
        cerr << "could not get transfer function " << fname << endl;
        cerr << flush;
        throw exception();
    }
    return tf;
}

int main(){

  for(int i=1; i<=1; i++){ //use to run same event multiple times
    cout << "Running event " << i << endl;

  //Load the transfer functions
  TFile* tffile = new TFile("root/transfer.root");

  //create a MEM configuration.
  //this needs to be done once per job, not for every event
  MEMConfig cfg;
  cfg.defaultCfg();
  cfg.transfer_function_method = TFMethod::External;
  vector<Permutations::Permutations> pvec({Permutations::BTagged, Permutations::QUntagged, Permutations::QQbarBBbarSymmetry});
  cfg.perm_pruning = pvec;
  cfg.save_permutations = false;
  //cfg.int_code = cfg.int_code + MEM::IntegrandType::AdditionalRadiation;
  cfg.int_code = (
    MEM::IntegrandType::IntegrandType::Constant |
    MEM::IntegrandType::IntegrandType::ScattAmpl |
    MEM::IntegrandType::IntegrandType::DecayAmpl |
    MEM::IntegrandType::IntegrandType::Jacobian |
    MEM::IntegrandType::IntegrandType::PDF |
    MEM::IntegrandType::IntegrandType::Transfer
  );
  cfg.perm_int = 0;
  cfg.integrator_type = MEM::IntegratorType::Vegas;
  cfg.eval_compiled_tf = true;
  cfg.interpolate_pdf = false;
  cfg.num_jet_variations = 100;
  //cfg.cuba_cores = 10;

  //Transfer functions for jet reconstruction efficiency
  cfg.set_tf_global(TFType::bLost, 0, getTransferFunction(tffile, "beff", 0.0));
  cfg.set_tf_global(TFType::bLost, 1, getTransferFunction(tffile, "beff", 2.0));
  cfg.set_tf_global(TFType::qLost, 0, getTransferFunction(tffile, "leff", 0.0));
  cfg.set_tf_global(TFType::qLost, 1, getTransferFunction(tffile, "leff", 2.0));

  //Create the mem integrator, once per job
  Integrand* integrand = new Integrand(0,cfg);
  
  //Add some objects to the MEM
  TLorentzVector lv_j1;
  TLorentzVector lv_j2;
  TLorentzVector lv_j3;
  TLorentzVector lv_j4;
  TLorentzVector lv_j5;
  TLorentzVector lv_j6;
  TLorentzVector lv_j7;
  TLorentzVector lv_j8;
  TLorentzVector lv_j9;
  TLorentzVector lv_l1;
  TLorentzVector lv_met;
  
  lv_l1.SetPtEtaPhiM(72.5140380859, -0.575425386429, 1.15235579014, -0.0196821801364);
  lv_met.SetPtEtaPhiM(68.7735290527, 0.0, -2.03213500977, 0.0);

  lv_j1.SetPtEtaPhiM(269.722503662, 1.36644697189, -1.33281302452, 55.2957420349);
  lv_j2.SetPtEtaPhiM(124.608589172, 0.271220892668, 2.05498027802, 16.2316188812);
  lv_j3.SetPtEtaPhiM(54.4055213928, 0.856117665768, -2.9505109787, 11.1097517014);
  lv_j4.SetPtEtaPhiM(36.7485122681, -0.152249529958, -2.50656199455, 7.3664727211);
  lv_j5.SetPtEtaPhiM(585.305664062, 1.7892742157, 1.43525516987, 47.8803405762);
  lv_j6.SetPtEtaPhiM(181.945419312, 0.429287433624, -1.10624468327, 18.4765701294);
//  lv_j7.SetPtEtaPhiM(155.970413208, 0.938717782497, -1.89792191982, 26.0079364777);
//  lv_j8.SetPtEtaPhiM(67.3059158325, 1.48887121677, -1.75795030594, 6.3827009201);

  // lv_j1.SetPtEtaPhiM( 139.711654663, 0.309017598629, 2.89014148712, 16.6812820435); //FH 7j4b
  // lv_j2.SetPtEtaPhiM( 55.4098548889, 0.535695493221, 2.02638745308, 10.5187835693);
  // lv_j3.SetPtEtaPhiM( 33.0288658142, 1.17760241032, -2.23624849319, 5.99395608902);
  // lv_j4.SetPtEtaPhiM( 176.863800049, 0.233719363809, -0.356812179089, 19.5611057281);
  // lv_j5.SetPtEtaPhiM( 95.4056777954, 1.95543289185, 0.659338533878, 11.2589387894);
  // lv_j6.SetPtEtaPhiM( 58.1257095337, 0.73320800066, -0.479299545288, 6.91729927063);
  // lv_j7.SetPtEtaPhiM( 43.062713623, 1.15584647655, -2.99698138237, 8.05434513092);
  // lv_met.SetPtEtaPhiM( 90.5287857056, 0.0, 3.13473391533, 0.0);

  // lv_j1.SetPtEtaPhiM( 176.253829956, -0.415994137526, 1.70584452152, 33.298625946 ); //FH 9j3b
  // lv_j2.SetPtEtaPhiM( 118.96509552, 0.336423158646, 1.1218277216, 12.3253850937 );
  // lv_j3.SetPtEtaPhiM( 57.4434776306, -0.441123634577, -0.160488530993, 11.2386312485 );
  // lv_j4.SetPtEtaPhiM( 30.3440284729, 1.19584238529, -2.33993601799, 7.37344026566 );
  // lv_j5.SetPtEtaPhiM( 152.161804199, 0.678951978683, -1.73020899296, 21.2293453217 );
  // lv_j6.SetPtEtaPhiM( 68.5895767212, -2.0358774662, -1.85538315773, 13.3149766922 );
  // lv_j7.SetPtEtaPhiM( 46.768119812, -0.142361089587, 2.72871279716, 6.65274238586 );
  // lv_j8.SetPtEtaPhiM( 40.6396408081, 1.30271017551, -1.33748292923, 8.26093196869 );
  lv_j9.SetPtEtaPhiM( 33.7472229004, 0.555128455162, -2.81841254234, 7.7496342659 );
  // lv_met.SetPtEtaPhiM( 34.4646186829, 0.0, 0.757133841515, 0.0);

  Object j1( lv_j1, ObjectType::Jet );
  j1.addObs( Observable::BTAG, 1. ); // 0 - jet is assumed to be from a light quark, 1 - a b quark
  j1.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j1.Eta()));
  j1.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j1.Eta()));
  
  Object j2( lv_j2, ObjectType::Jet );
  j2.addObs( Observable::BTAG, 1. );
  j2.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j2.Eta()));
  j2.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j2.Eta()));

  Object j3( lv_j3, ObjectType::Jet );
  j3.addObs( Observable::BTAG, 1. );
  j3.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j3.Eta()));
  j3.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j3.Eta()));

  Object j4( lv_j4, ObjectType::Jet );
  j4.addObs( Observable::BTAG, 1. ); //DS: 3or4 bs
  j4.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j4.Eta()));
  j4.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j4.Eta()));
  
  Object j5( lv_j5, ObjectType::Jet );
  j5.addObs( Observable::BTAG, 0. );
  j5.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j5.Eta()));
  j5.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j5.Eta()));

  Object j6( lv_j6, ObjectType::Jet );
  j6.addObs( Observable::BTAG, 0. );
  j6.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j6.Eta()));
  j6.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j6.Eta()));

//  Object j7( lv_j7, ObjectType::Jet );
//  j7.addObs( Observable::BTAG, 0. );
//  j7.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j7.Eta()));
//  j7.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j7.Eta()));
//  
//  Object j8( lv_j8, ObjectType::Jet );
//  j8.addObs( Observable::BTAG, 0. );
//  j8.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j8.Eta()));
//  j8.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j8.Eta()));
//
//  Object j9( lv_j9, ObjectType::Jet ); //DS
//  j9.addObs( Observable::BTAG, 0. );
//  j9.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j9.Eta()));
//  j9.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j9.Eta()));

  for (int i=0; i<100; i++) {
    double corr = 0.9 + 0.2*(float)i/100.0;
    j1.p4_variations.push_back(corr);
    j2.p4_variations.push_back(corr);
    j3.p4_variations.push_back(corr);
    j4.p4_variations.push_back(corr);
    j5.p4_variations.push_back(corr);
    j6.p4_variations.push_back(corr);
//    j7.p4_variations.push_back(corr);
//    j8.p4_variations.push_back(corr);
//    j9.p4_variations.push_back(corr);
  }
  Object l1( lv_l1, ObjectType::Lepton );
  l1.addObs( Observable::CHARGE, -1. );

  //create a MET
  Object met( lv_met, ObjectType::MET );
  
  for (int i=0; i < MAX_TIMES; i++) {
    //add all objects to the MEM integrator
    integrand->push_back_object( &j1 );
    integrand->push_back_object( &j2 );
    integrand->push_back_object( &j3 );
    integrand->push_back_object( &j4 );
    integrand->push_back_object( &j5 );
    integrand->push_back_object( &j6 );
    //integrand->push_back_object( &j7 );
    //integrand->push_back_object( &j8 );
    //integrand->push_back_object( &j9 );
    integrand->push_back_object( &l1 );
    integrand->push_back_object( &met );

    MEMOutput res;
   
    double p0, p1, mem_w;
    
    bool do_full = false;
    bool do_1122 = false;
    bool do_421 = false;
    bool do_122 = false;
    bool do_022 = true;
    const auto finalstate = FinalState::LH;

    if (do_full) {
      //Evaluate fully reconstructed hypothesis
      cout << "Fully reconstructed interpretation" << endl;
      cout << "evaluating tth hypo" << endl;
      //third variable is variables to integrate over
      //if nothing is specified, assume that all jets (4b + 2 light) were reconstructed
      res = integrand->run(finalstate, Hypothesis::TTH,  {}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p0 = res.p;

      cout << "evaluating ttbb hypo" << endl;
      res = integrand->run(finalstate, Hypothesis::TTBB, {}, {}, npoints  );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p1 = res.p;

      //this is the final discriminator value. the normalization constant is to be optimized
      mem_w = p0 / (p0 + 0.1*p1);
      cout << "mem 222 discriminator " << mem_w << endl;
    }
    if (do_1122) { 
      //Evaluate 1122 hypothesis. We are missing two l-quarks.
      cout << "1122" << endl;
      cout << "evaluating tth hypo" << endl;
      //integrate over the b-quark angles
      res = integrand->run(finalstate, Hypothesis::TTH, {}, {PSVar::cos_qbar1, PSVar::phi_qbar1, PSVar::cos_qbar2, PSVar::phi_qbar2}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p0 = res.p;
      
      cout << "evaluating ttbb hypo" << endl;
      res = integrand->run(finalstate, Hypothesis::TTBB, {}, {PSVar::cos_qbar1, PSVar::phi_qbar1, PSVar::cos_qbar2, PSVar::phi_qbar2}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p1 = res.p;
      mem_w = p0 / (p0 + 0.02*p1);
      cout << "mem 1122 discriminator " << mem_w << endl;
    }
    if (do_421) { 
      //Evaluate 421 hypothesis. We are missing a b-quark.
      cout << "421" << endl;
      cout << "evaluating tth hypo" << endl;
      //integrate over the b-quark angles
      res = integrand->run(finalstate, Hypothesis::TTH,  {PSVar::cos_b1, PSVar::phi_b1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p0 = res.p;
      
      cout << "evaluating ttbb hypo" << endl;
      res = integrand->run(finalstate, Hypothesis::TTBB, {PSVar::cos_b1, PSVar::phi_b1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p1 = res.p;
      mem_w = p0 / (p0 + 0.1*p1);
      cout << "mem 421 discriminator " << mem_w << endl;
    }
    if (do_122) {
      //Evaluate 122 hypothesis. We do not use the information provided by the light quarks.
      cout << "122" << endl;
      cout << "evaluating tth hypo" << endl;
      //integrate over the light quark angles
      res = integrand->run(finalstate, Hypothesis::TTH,  {PSVar::cos_qbar1, PSVar::phi_qbar1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p0 = res.p;
      
      cout << "evaluating ttbb hypo" << endl;
      res = integrand->run(finalstate, Hypothesis::TTBB, {PSVar::cos_qbar1, PSVar::phi_qbar1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p1 = res.p;
      mem_w = p0 / (p0 + 0.1*p1);
      cout << "mem 122 discriminator " << mem_w << endl;
    }
    if (do_022) {
      //Evaluate 022 hypothesis. We do not use the information provided by the light quarks.
      cout << "022" << endl;
      cout << "evaluating tth hypo" << endl;
      //integrate over the light quark angles
      res = integrand->run(finalstate, Hypothesis::TTH,  {PSVar::cos_q1, PSVar::phi_q1, PSVar::cos_qbar1, PSVar::phi_qbar1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p0 = res.p;
      
      cout << "evaluating ttbb hypo" << endl;
      res = integrand->run(finalstate, Hypothesis::TTBB, {PSVar::cos_q1, PSVar::phi_q1, PSVar::cos_qbar1, PSVar::phi_qbar1}, {}, npoints );
      cout << "p = " << res.p << " +- " << res.p_err << endl;
      p1 = res.p;
      
      mem_w = p0 / (p0 + 0.1*p1);
      cout << "mem 022 discriminator " << mem_w << endl;
    }
  cout << "N " << integrand->n_calls_perm << " " << integrand->scattering_counter << endl;
  integrand->next_event();
  }

  delete integrand;
}
}
