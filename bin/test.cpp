#include "TTH/MEIntegratorStandalone/interface/Integrand.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/GSLMCIntegrator.h"
#include "Math/IOptions.h"
#include "Math/IntegratorOptions.h"
#include "Math/AllIntegrationTypes.h"
#include "TFile.h"
#include "TStopwatch.h"
#include <iostream>

using namespace std;
using namespace MEM;

// Returns the transfer function corresponding to a jet flavour and eta
TF1* getTransferFunction(TFile* tffile, const char* flavour, double eta) {
    int etabin = 0;
    if (std::abs(eta) > 1.0) {
        etabin = 1;
    }
    stringstream ss;
    ss << "tf_" << flavour << "_etabin" << etabin;
    const char* fname = ss.str().c_str();
    TF1* tf = (TF1*)(tffile->Get(fname));
    if (tf == 0) {
        cerr << "could not get transfer function " << fname << endl;
        cerr << flush;
        throw exception();
    }
    return tf;
}

extern "C" {
  void ol_setparameter_int(const char* param, int val);
  void ol_setparameter_double(const char* param, double val);
  int ol_register_process(const char* process, int amptype);
  int ol_n_external(int id);
  void ol_phase_space_point(int id, double sqrt_s, double* pp);
  void ol_start();
  void ol_finish();
  void ol_evaluate_tree(int id, double* pp, double* m2_tree);
  void ol_evaluate_loop(int id, double* pp, double* m2_tree, double* m2_loop, double* acc);
}

int main(){
  //Load the transfer functions
  TFile* tffile = new TFile("root/transfer.root");

  //create a MEM configuration.
  //this needs to be done once per job, not for every event
  MEMConfig cfg;
  cfg.defaultCfg();
  cfg.transfer_function_method = TFMethod::External;
  vector<Permutations::Permutations> pvec({Permutations::BTagged, Permutations::QUntagged, Permutations::QQbarBBbarSymmetry});
  cfg.perm_pruning = pvec;
  cfg.save_permutations = false;
  //cfg.int_code = cfg.int_code + MEM::IntegrandType::AdditionalRadiation;
  cfg.int_code = (
    MEM::IntegrandType::IntegrandType::Constant |
    MEM::IntegrandType::IntegrandType::ScattAmpl |
    MEM::IntegrandType::IntegrandType::DecayAmpl |
    MEM::IntegrandType::IntegrandType::Jacobian |
    MEM::IntegrandType::IntegrandType::PDF |
    MEM::IntegrandType::IntegrandType::Transfer
  );
  cfg.perm_int = 0;
  cfg.integrator_type = MEM::IntegratorType::Vegas;
  cfg.eval_compiled_tf = true;
  cfg.interpolate_pdf = false;
  cfg.num_jet_variations = 100;
  
  //Transfer functions for jet reconstruction efficiency
  cfg.set_tf_global(TFType::bLost, 0, getTransferFunction(tffile, "beff", 0.0));
  cfg.set_tf_global(TFType::bLost, 1, getTransferFunction(tffile, "beff", 2.0));
  cfg.set_tf_global(TFType::qLost, 0, getTransferFunction(tffile, "leff", 0.0));
  cfg.set_tf_global(TFType::qLost, 1, getTransferFunction(tffile, "leff", 2.0));
  
  //Create the mem integrator, once per job
  Integrand* integrand = new Integrand(0,cfg);
  
  //Add some objects to the MEM
  TLorentzVector lv_j1;
  lv_j1.SetPtEtaPhiM(72.5140380859, -0.575425386429, 1.15235579014, -0.0196821801364);
  Object j1( lv_j1, ObjectType::Jet );
  j1.addObs( Observable::BTAG, 1. ); // 0 - jet is assumed to be from a light quark, 1 - a b quark
  j1.addTransferFunction(TFType::bReco, getTransferFunction(tffile, "b", lv_j1.Eta()));
  j1.addTransferFunction(TFType::qReco, getTransferFunction(tffile, "l", lv_j1.Eta()));
  for (int i=0; i<100; i++) {
    double corr = 0.9 + 0.2*(float)i/100.0;
    j1.p4_variations.push_back(corr);
  }

  double x[] = {100.0};
  std::vector<double> variations;
  for (int i=0; i<1000; i++) {
    x[0] = 30.0 + 370.0/1000.0 * i;
    double w1 = transfer_function2(&j1, x, TFType::qReco, variations, false);
    double w2 = transfer_function2(&j1, x, TFType::bReco, variations, false);
    std::cout << x[0] << " " << w1 << " " << w2 << std::endl;
  }

  for (int j=0; j<3; j++) {
    TStopwatch sw;
    sw.Start();
    double wtot = 0;
    for (int i=0; i<1000000; i++) {
      x[0] = 300.0 * rand();
      wtot += transfer_function2(&j1, x, TFType::qReco, variations, true);
    }
    sw.Stop();
    cout << "TF eval " << sw.CpuTime() << " seconds / 1M" << endl;
  }
  
  int id = ol_register_process("21 21 -> 6 -6 5 -5", 1);
  double pp[5*ol_n_external(id)];
  double sqrts = 13000.0;
  double m2_tree = 0.0;
  for (int j=0; j<3; j++) {
    TStopwatch sw;
    sw.Start();
    double wtot = 0;
    for (int i=0; i<1000000; i++) {
      ol_phase_space_point(id, sqrts, pp);
      ol_evaluate_tree(id, pp, &m2_tree);
    }
    cout << "ME eval " << sw.CpuTime() << " seconds / 1M" << endl;
  }

  return 0;
}
