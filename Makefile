all: tests
tests: data/*.cfg

#tests
%.cfg:
	./python/run.py $@ > logs/$(*F).log

.PHONY: data/*.cfg
